team join dontmindme @e[type=minecraft:ghast,team=!dontmindme]
team join dontmindme @e[type=minecraft:creeper,team=!dontmindme]
team join dontmindme @a[gamemode=adventure,team=!dontmindme]
team leave @a[gamemode=!adventure,team=dontmindme]
effect give @a[gamemode=adventure] minecraft:saturation 2 255 true
effect give @a[gamemode=adventure] minecraft:resistance 2 4 true
execute as @a[gamemode=adventure,scores={swimming=1..}] run function visitors:swimming
execute as @a[gamemode=adventure,scores={swimming=0}] run function visitors:standing
scoreboard players set @a[gamemode=adventure] swimming 0
scoreboard players reset @a[gamemode=!adventure] swimming
kill @e[type=minecraft:falling_block,tag=placeholder,nbt={Time:1}]
