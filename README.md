# Minecraft visitor mode
Welcome to Minecraft visitor mode datapacks page!

## What is it:
This repository is custom Minecraft data pack which bends rules of adventure mode so anybody can safely host visitors on their server without fear of them interacting with things they should not.

## Version
Pack was written and tested in Minecraft version 1.18.2 but it should be able to run on almost any version of Minecraft supporting datapacks.

## Get started
To make the mode running propperly do these things:
- edit server rules for new players to make them adventure mode
- copy `visitor-mode.zip` to datapacks folder
- either restart server or run `/datapacks enable file/visitor-mode`

Now everything should be up and running. To verify that just switch yourself to `adventure` gamemode and try to push some buttons or punch friends :)

## Known issues
The datapack is not perfect but it can do a lot. Here are issues I discovered:
- sometimes when server is lagging behind player can on brief moment interact with things while sprinting and jumping
- players still can interact with pressure plates *(Not sure I can fix that with datapack)*

# ToDo
- [ ] Try datapack on 3rd party server hosting
- [x] Find out how to prevent squished players to interact
- [ ] ~~Enhance `load.mcfunction` so it kills border entities~~
    - After consideration this does not make sense because it would affect only spawn chunks. Either way after 100 ticks these entities despawn itself.
- [x] Make border entities not dropping items when killed by timeout
- [ ] Test whether there is way to enhance blocking feature
